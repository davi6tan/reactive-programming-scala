package kvstore

import akka.testkit.{ TestProbe, TestKit, ImplicitSender }
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers
import org.scalatest.FunSuiteLike
import akka.actor.ActorSystem
import scala.concurrent.duration._
import kvstore.Arbiter.{ JoinedSecondary, Join }
import kvstore.Persistence.{ Persisted, Persist }
import scala.util.Random
import scala.util.control.NonFatal

class Step2_SecondarySpec extends TestKit(ActorSystem("Step2SecondarySpec"))
  with FunSuiteLike
  with BeforeAndAfterAll
  with Matchers
  with ImplicitSender
  with Tools {

  override def afterAll(): Unit = {
    system.shutdown()
  }

  test("case1: Secondary (in isolation) should properly register itself to the provided Arbiter") {
    val arbiter = TestProbe()
    val secondary = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = false)), "case1-secondary")

    arbiter.expectMsg(Join)
  }

  test("case2: Secondary (in isolation) must handle Snapshots") {
    import Replicator._

    val arbiter = TestProbe()
    val replicator = TestProbe()
    val secondary = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = false)), "case2-secondary")
    val client = session(secondary)

    arbiter.expectMsg(Join)
    arbiter.send(secondary, JoinedSecondary)

    client.get("k1") should === (None)

    replicator.send(secondary, Snapshot("k1", None, 0L))
    replicator.expectMsg(SnapshotAck("k1", 0L))
    client.get("k1") should === (None)

    replicator.send(secondary, Snapshot("k1", Some("v1"), 1L))
    replicator.expectMsg(SnapshotAck("k1", 1L))
    client.get("k1") should === (Some("v1"))

    replicator.send(secondary, Snapshot("k1", None, 2L))
    replicator.expectMsg(SnapshotAck("k1", 2L))
    client.get("k1") should === (None)
  }

  test("case3: Secondary should drop and immediately ack snapshots with older sequence numbers") {
    import Replicator._

    val arbiter = TestProbe()
    val replicator = TestProbe()
    val secondary = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = false)), "case3-secondary")
    val client = session(secondary)

    arbiter.expectMsg(Join)
    arbiter.send(secondary, JoinedSecondary)

    client.get("k1") should === (None)

    replicator.send(secondary, Snapshot("k1", Some("v1"), 0L))
    replicator.expectMsg(SnapshotAck("k1", 0L))
    client.get("k1") should === (Some("v1"))

    replicator.send(secondary, Snapshot("k1", None, 0L))
    replicator.expectMsg(SnapshotAck("k1", 0L))
    client.get("k1") should === (Some("v1"))

    replicator.send(secondary, Snapshot("k1", Some("v2"), 1L))
    replicator.expectMsg(SnapshotAck("k1", 1L))
    client.get("k1") should === (Some("v2"))

    replicator.send(secondary, Snapshot("k1", None, 0L))
    replicator.expectMsg(SnapshotAck("k1", 0L))
    client.get("k1") should === (Some("v2"))
  }

  test("case4: Secondary should drop snapshots with future sequence numbers") {
    import Replicator._

    val arbiter = TestProbe()
    val replicator = TestProbe()
    val secondary = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = false)), "case4-secondary")
    val client = session(secondary)

    arbiter.expectMsg(Join)
    arbiter.send(secondary, JoinedSecondary)

    client.get("k1") should === (None)

    replicator.send(secondary, Snapshot("k1", Some("v1"), 1L))
    replicator.expectNoMsg(300.milliseconds)
    client.get("k1") should === (None)

    replicator.send(secondary, Snapshot("k1", Some("v2"), 0L))
    replicator.expectMsg(SnapshotAck("k1", 0L))
    client.get("k1") should === (Some("v2"))
  }

//--
/*
  test("case5: Secondary should behave like a standard map", 35) {
    val arbiter = TestProbe()
    val secondary = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = false)), "case5-secondary")

    val rnd = new Random(23456)
    val keys = (0 to 9).map("k" + _).toVector
    def key = keys(rnd.nextInt(10))
    def value = (rnd.nextInt(9000) + 1000).toString

    arbiter.expectMsg(Join)
    arbiter.send(secondary, JoinedSecondary)

    import Replicator._
    var log = Vector.empty[Snapshot]
    var map = Map.empty[String, String]
    try {
      (0 to 100) foreach { id =>
        val k = key
        val op =
          if (rnd.nextFloat < 0.3) {
            map -= k
            Snapshot(k, None, id)
          } else {
            val v = value
            map += k -> v
            Snapshot(k, Some(v), id)
          }
        log :+= op
        secondary ! op
        expectMsg(SnapshotAck(k, id))
        val k2 = key
        secondary ! Replica.Get(k2, id)
        expectMsg(Replica.GetResult(k2, map get k2, id))
      }
    } catch {
      case NonFatal(e) =>
        info(log.mkString("operations log:\n", "\n", ""))
        throw e
    }
  }
  */
/*++++++*/

}
/*
 * Run starting. Expected test count is: 4
Step2_SecondarySpec:
- case1: Secondary (in isolation) should properly register itself to the provided Arbiter
- case2: Secondary (in isolation) must handle Snapshots
- case3: Secondary should drop and immediately ack snapshots with older sequence numbers
- case4: Secondary should drop snapshots with future sequence numbers
Run completed in 921 milliseconds.
Total number of tests run: 4
Suites: completed 1, aborted 0
Tests: succeeded 4, failed 0, canceled 0, ignored 0, pending 0
All tests passed.
 * 
 */
