package calculator

import Signal._

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double], c: Signal[Double]): Signal[Double] = {
    Signal({
      val b_val = b()
      (b_val * b_val) - (4 * a() * c())
    })
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double], c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal({
      def compute(delta_v: Double): Double = {
        ((-b()) + delta_v) / (2 * a())
      }
      delta() match {
        case x if x > 0 => val sqrt = Math.sqrt(x); Set(compute(-sqrt), compute(sqrt))
        case y if y == 0 => Set(compute(0))
        case _ => Set()
      }
    })
  }
}
